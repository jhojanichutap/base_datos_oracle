# 2. Ingresar registros (insert into- select)

Un registro es una fila de la tabla que contiene los datos propiamente dichos. Cada registro tiene un dato por cada columna (campo). Nuestra tabla "usuarios" consta de 2 campos, "nombre" y "clave".

Al ingresar los datos de cada registro debe tenerse en cuenta la cantidad y el orden de los campos.

La sintaxis básica y general es la siguiente:

```sql
 insert into NOMBRETABLA (NOMBRECAMPO_1, ..., NOMBRECAMPO_n) values (VALORCAMPO_1, ..., VALORCAMPO_n);
```

Usamos "insert into", luego el nombre de la tabla, detallamos los nombres de los campos entre paréntesis y separados por comas y luego de la cláusula "values" colocamos los valores para cada campo, también entre paréntesis y separados por comas.

En el siguiente ejemplo se agrega un registro a la tabla "usuarios", en el campo "nombre" se almacenará "Mariano" y en el campo "clave" se guardará "payaso":

```sql
insert into usuarios (nombre, clave) values ('Mariano','payaso');
```

Luego de cada inserción aparece un mensaje indicando la cantidad de registros ingresados.

Note que los datos ingresados, como corresponden a cadenas de caracteres se colocan entre comillas simples.

Para ver los registros de una tabla usamos "select":

```sql
select *from usuarios;
```

El comando "select" recupera los registros de una tabla. Con el asterisco indicamos que muestre todos los campos de la tabla "usuarios".

Aparece la tabla, sus campos y registros ingresados; si no tiene registros, aparecerían solamente los campos y la tabla vacía.

Es importante ingresar los valores en el mismo orden en que se nombran los campos: En el siguiente ejemplo se lista primero el campo "clave" y luego el campo "nombre" por eso, los valores también se colocan en ese orden:

```sql
insert into usuarios (clave, nombre) values ('River','Juan');
```

Si ingresamos los datos en un orden distinto al orden en que se nombraron los campos, no aparece un mensaje de error y los datos se guardan de modo incorrecto.

En el siguiente ejemplo se colocan los valores en distinto orden en que se nombran los campos, el valor de la clave (la cadena "Boca") se guardará en el campo "nombre" y el valor del nombre (la cadena "Luis") en el campo "clave":

```sql
insert into usuarios (nombre,clave) values ('Boca','Luis');
```

## Ejercicios de laboratorio

Vemos si la tabla "usuarios" existe:

```sql
select *from all_tables;
```

Si existe la eliminamos:

```sql
drop table usuarios;
```

Creamos una nueva tabla denominada "usuarios" con los siguientes campos:

```sql
create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);
```

Veamos si tiene registros:

```sql
select *from usuarios;
```

No tiene, la tabla aparece vacía, solamente vemos las columnas que muestran los nombres de sus campos.

Agregamos un registro a la tabla:

```sql
insert into usuarios (nombre, clave) values ('Mariano','payaso');
```

Un mensaje indica que se ingreso una fila.

Veamos nuevamente los registros de la tabla "usuarios":

```sql
 select *from usuarios;
```

Aparece la siguiente tabla:

```sh
NOMBRE CLAVE
-------------

Mariano payaso
```

La tabla contiene un solo registro, el ingresado recientemente.

Ingresamos otro registro, esta vez cambiamos el orden de los campos:

```sql
insert into usuarios (clave, nombre) values ('River','Juan');
```

Ingresamos los datos en un orden distinto al orden en que se nombran los campos, no aparece un mensaje de error y los datos se guardan de modo incorrecto:

```sql
insert into usuarios (nombre,clave) values ('Boca','Luis');
```

Veamos cómo se almacenaron los datos:

```sql
select *from usuarios;
```

Aparece la siguiente tabla:

```sh
NOMBRE CLAVE
-------------

Mariano payaso
Juan River
Boca Luis
```

La tabla tiene 3 registros. Note que la clave "Boca" se guardó en el campo "nombre" y el nombre de usuario "Luis" en el campo "clave".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
select * from all_tables;

drop table usuarios;

create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

select * from usuarios;

insert into usuarios (nombre, clave) values ('Mariano','payaso');

select *from usuarios;

insert into usuarios (clave, nombre) values ('River','Juan');

insert into usuarios (nombre,clave) values ('Boca','Luis');

select * from usuarios;
```

La ejecución de este lote de comandos SQL genera una salida similar a:

![1](imagenes/2/1.jpg)

## Ejercicios propuestos

### Ejercicio 01

Trabaje con la tabla "agenda" que almacena información de sus amigos.

1. Elimine la tabla "agenda"

2. Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)

3. Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)

4. Visualice la estructura de la tabla "agenda" (describe)

5. Ingrese los siguientes registros:

```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');
```

6. Seleccione todos los registros de la tabla.

7. Elimine la tabla "agenda"

8. Intente eliminar la tabla nuevamente (aparece un mensaje de error)

### Ejercicio 02

Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.

1. Elimine la tabla "libros"

2. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)

3. Visualice las tablas existentes.

4. Visualice la estructura de la tabla "libros"

Muestra los campos y los tipos de datos de la tabla "libros".

5. Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');
```

6. Muestre todos los registros (select) de "libros"
